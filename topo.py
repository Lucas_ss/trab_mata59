#!/usr/bin/python   
#coding=UTF-8                                                                         
                                                                                        
from mininet.topo import Topo
from mininet.net import Mininet
from mininet.util import dumpNodeConnections
from mininet.cli import CLI
from mininet.node import RemoteController

class MyTopo(Topo):
    def __init__(self,**opts):
        # Initialize topology and default options
        Topo.__init__(self, **opts)

        #Adiciona o switch 's1' a topologia.
        switch = self.addSwitch('s1')

        #Adiciona hosts h1 e h2 a topologia.
        for h in xrange(2):
            host = self.addHost('h%d' % (h + 1))

        #Adiciona Links entre h1,s1 e h2.
        self.addLink('h1','s1')
        self.addLink('h2','s1')


def main():
    topo = MyTopo()
    net = Mininet(topo = topo, controller = RemoteController) 
    net.start()

    s1 = net.get('s1') #Get switch s1 instance from net
    
    #Adicionando no switch os fluxos via 'ovs-ofctl add-flow'
    # formato do comando:
    # ovs-ofctl add-flow [switch] [regras], [ações]
    #s1.cmd('ovs-ofctl add-flow s1 in_port=1,actions=output:2') 
    #s1.cmd('ovs-ofctl add-flow s1 in_port=2,actions=output:1')
    "Descomentando as linhas 36 e 37, os hosts devem ser capazes de enviarem pacotes um pro outro,\
    vocês podem testar usando ping, utilizando o comando h1 ping h2 na CLI do mininet" 

    print "Testing network connectivity"
    net.pingAll()


    #---------- ARP ------------------------------------------------------------------#
    #Vocês precisarão dessa parte do codigo para não precisarem tratar o problema
    #de loops na rede no broadcast do protocolo ARP. basta copiar e colar, eestá feito
    #para configurar uma rede com 9 hosts (que é o cas do trabalho).
    #Basta descomentar as proximas 5 linhas no script da topologia de vocês.
    #for i in xrange(9):
    #    h = net.get('h%d' % (i + 1))
    #    h.cmd("ip route add default dev %s-eth0" % ('h%d' % (i + 1)))
    #    for j in xrange(9):
    #        h_dst = net.get('h%d' % (j+1))
    #        h.setARP(h_dst.IP(), h_dst.MAC())
    #---------------------------------------------------------------------------------#


    CLI(net)
    net.stop()

if __name__ == '__main__':
    main()
